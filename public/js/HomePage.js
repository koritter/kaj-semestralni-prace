import AbstractView from "./AbstractView.js";

/**
 * Class representing the homepage view of the Reversi application.
 * @extends AbstractView
 */
export default class HomePage extends AbstractView {
    /**
     * Creates an instance of the homepage view.
     * @param {Object} settings - The settings for the view.
     */
    constructor(settings) {
        super(settings);
    }

    /**
     * Generates the HTML for the homepage view.
     * @returns {string} The HTML string for the homepage view.
     */
    getHtml() {
        return `
        <section id="homepage">
        <h1>Rozcestník - Reversi</h1>          
        <nav>
            <ul>
                <li><a href="?page=Singleplayer">Singleplayer</a></li>
                <li><a href="?page=Multiplayer">Multiplayer</a></li>
                <li><a href="?page=AboutGame">O hře</a></li>
                <li><a href="?page=BestPlayers">Nejlepší hráči</a></li>
            </ul>
        </nav>
        </section>
        `;
    }
}
