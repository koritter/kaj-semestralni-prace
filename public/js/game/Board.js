import SoundManager from "../api-managers/SoundManager.js";

/**
 * Represents the game board for a Reversi game.
 */
export default class Board {
    /**
     * Creates an instance of the Board class.
     * @param {boolean} showHelp - Indicates whether to show valid moves.
     */
    constructor(showHelp) {
        this.board = Array.from(Array(8), () => Array(8).fill(0));

        this.board[3][3] = 2; // 2 - white, 1 - black
        this.board[4][4] = 2;
        this.board[3][4] = 1;
        this.board[4][3] = 1;

        this.canvas = document.querySelector('#gameBoard');

        this.score = {
            white: 2,
            black: 2,
            no: 60
        };

        this.help = showHelp;

        this.bgimage = new Image();
        this.bgimage.src = "./img/boardbg.png";

        this.whiteStone = new Image();
        this.whiteStone.src = "./img/whitestone.png";

        this.blackStone = new Image();
        this.blackStone.src = "./img/blackstone.png";

        this.mouseX = 0;
        this.mouseY = 0;
        this.canvas.addEventListener('mouseleave', this.handleMouseLeave.bind(this));
        this.canvas.addEventListener('mousemove', this.handleMouseMove.bind(this));

        this.player = -1;

        this.soundManager = new SoundManager();
        this.notePlaying = false;
    }

    /**
     * Handles mouse move events.
     * @param {MouseEvent} event - The mouse move event.
     */
    handleMouseMove(event) {
        const rect = this.canvas.getBoundingClientRect();
        this.mouseX = event.clientX - rect.left;
        this.mouseY = event.clientY - rect.top;

        const tileSize = 50;
        const mousePosX = Math.floor(this.mouseX / tileSize);
        const mousePosY = Math.floor(this.mouseY / tileSize);

        const validMoves = this.getValidMoves(this.player);
        let isMouseOverValidMove = false;
        for (const move of validMoves) {
            if (move[0] === mousePosY && move[1] === mousePosX) {
                isMouseOverValidMove = true;

                break;
            }
        }

        this.canvas.style.cursor = (isMouseOverValidMove && this.help) ? 'pointer' : 'default';
        
        if (isMouseOverValidMove && !this.notePlaying && this.help) {
            this.soundManager.playNote('C4', 1, 'square');
            this.notePlaying = true;
        } else if (!isMouseOverValidMove && this.notePlaying && this.help) {
            this.notePlaying = false;
        }

    }


    /**
     * Determines if any player can make a move.
     * @returns {boolean} True if any player can play a move, false otherwise.
     */
    canAnyonePlay() {
        return this.getValidMoves(1).length != 0 || this.getValidMoves(2).length != 0;
    }

    /**
     * Handles mouse leave events.
     */
    handleMouseLeave() {
        this.canvas.style.cursor = 'default';
        this.mouseX = 0;
        this.mouseY = 0;
    }

    /**
     * Draws the game board.
     * @param {CanvasRenderingContext2D} ctx - The canvas rendering context.
     */
    drawBoard(ctx) {
        const tileSize = 50;
        const width = tileSize * this.board.length;
        const height = tileSize * this.board.length;

        this.canvas.width = width;
        this.canvas.height = height;

        if (this.bgimage.complete) {
            ctx.drawImage(this.bgimage, 0, 0, width, height);
        } else {
            this.bgimage.onload = () => {
                ctx.drawImage(this.bgimage, 0, 0, width, height);
            };
        }
    }

    /**
     * Draws the stones on the game board.
     * @param {CanvasRenderingContext2D} ctx - The canvas rendering context.
     */
    drawStones(ctx) {
        const tileSize = 50;

        for (let i = 0; i < this.board.length; i++) {
            for (let j = 0; j < this.board[i].length; j++) {
                const x = j * tileSize;
                const y = i * tileSize;

                if (this.board[i][j] == 2) {
                    if (this.whiteStone.complete) {
                        ctx.drawImage(this.whiteStone, x + 10, y + 10, tileSize - 20, tileSize - 20);
                    } else {
                        this.whiteStone.onload = () => {
                            ctx.drawImage(this.whiteStone, x + 10, y + 10, tileSize - 20, tileSize - 20);
                        };
                    }
                } else if (this.board[i][j] == 1) {
                    if (this.blackStone.complete) {
                        ctx.drawImage(this.blackStone, x + 10, y + 10, tileSize - 20, tileSize - 20);
                    } else {
                        this.blackStone.onload = () => {
                            ctx.drawImage(this.blackStone, x + 10, y + 10, tileSize - 20, tileSize - 20);
                        };
                    }
                }
            }
        }
    }

    /**
     * Draws the valid moves on the game board.
     * @param {CanvasRenderingContext2D} ctx - The canvas rendering context.
     * @param {number} player - The player number (1 for black, 2 for white).
     */
    drawValidMoves(ctx, player) {
        let valid_moves = this.getValidMoves(player);
        const tileSize = 50;
        const mousePosX = Math.floor(this.mouseX / tileSize);
        const mousePosY = Math.floor(this.mouseY / tileSize);

        for (let i = 0; i < valid_moves.length; i++) {
            let radius = (tileSize - 20) / 3;
            if (valid_moves[i][1] == mousePosX && valid_moves[i][0] == mousePosY) {
                radius = (tileSize - 20) / 2.7;
            }

            const x = valid_moves[i][1] * tileSize + tileSize / 2;
            const y = valid_moves[i][0] * tileSize + tileSize / 2;

            ctx.beginPath();
            ctx.arc(x, y, radius, 0, Math.PI * 2);
            ctx.strokeStyle = 'rgba(58,171,142, 0.8)';
            ctx.lineWidth = 6.5;
            ctx.shadowColor = 'rgba(105,238,204, 1)';
            ctx.shadowBlur = 5;
            ctx.stroke();
            ctx.closePath();
        }
    }

    /**
     * Draws the complete game board with stones and valid moves.
     * @param {number} activePlayer - The active player number (1 for black, 2 for white).
     */
    drawCompleteBoard(activePlayer) {
        this.player = activePlayer;
        const ctx = this.canvas.getContext('2d');

        this.drawBoard(ctx);
        this.drawStones(ctx);

        if (this.help) {
            this.drawValidMoves(ctx, activePlayer);
        }
    }

    /**
     * Gets the valid moves for a player.
     * @param {number} player - The player number (1 for black, 2 for white).
     * @returns {Array} An array of valid move coordinates.
     */
    getValidMoves(player) {
        let valid_moves = [];

        for (let x = 0; x < this.board.length; x++) {
            for (let y = 0; y < this.board[x].length; y++) {
                if (this.board[x][y] == 0 && this.isValidMove([x, y], player)) {
                    valid_moves.push([x, y]);
                }
            }
        }

        return valid_moves;
    }

    /**
     * Checks if a move is valid for a player.
     * @param {Array} move - The move coordinates [x, y].
     * @param {number} player - The player number (1 for black, 2 for white).
     * @returns {boolean} True if the move is valid, false otherwise.
     */
    isValidMove(move, player) {
        if (this.board[move[0]][move[1]] == 0) {
            let directions = [
                [-1, -1], [-1, 0], [-1, 1],
                [0, -1], [0, 1],
                [1, -1], [1, 0], [1, 1]
            ];

            for (let i = 0; i < directions.length; i++) {
                if (this.checkDirection(move, directions[i], player)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Checks if there are stones to flip in a direction.
     * @param {Array} move - The move coordinates [x, y].
     * @param {Array} direction - The direction to check [dx, dy].
     * @param {number} player - The player number (1 for black, 2 for white).
     * @returns {boolean} True if there are stones to flip, false otherwise.
     */
    checkDirection(move, direction, player) {
        let newX = move[0] + direction[0];
        let newY = move[1] + direction[1];
        let firstIter = true;

        if (newX >= 0 && newX < this.board.length && newY >= 0 && newY < this.board.length) {
            while (newX >= 0 && newX < this.board.length && newY >= 0 && newY < this.board.length) {
                if (this.board[newX][newY] == 0) {
                    return false;
                }
                if (firstIter) {
                    if (this.board[newX][newY] == player) {
                        return false;
                    }
                    firstIter = false;
                } else if (this.board[newX][newY] == player) {
                    return true;
                }

                newX += direction[0];
                newY += direction[1];
            }
        }

        return false;
    }

    /**
     * Plays a move for a player.
     * @param {Array} move - The move coordinates [x, y].
     * @param {number} player - The player number (1 for black, 2 for white).
     */
    playPlayersMove(move, player) {
        this.board[move[0]][move[1]] = player;

        let directions = [
            [-1, -1], [-1, 0], [-1, 1],
            [0, -1], [0, 1],
            [1, -1], [1, 0], [1, 1]
        ];

        for (let i = 0; i < directions.length; i++) {
            if (this.checkDirection(move, directions[i], player)) {
                this.recolorStonesInDirection(move, directions[i], player);
            }
        }

        this.updateScore();
    }

    /**
     * Recolors stones in a direction.
     * @param {Array} move - The move coordinates [x, y].
     * @param {Array} direction - The direction to recolor [dx, dy].
     * @param {number} player - The player number (1 for black, 2 for white).
     */
    recolorStonesInDirection(move, direction, player) {
        let newX = move[0] + direction[0];
        let newY = move[1] + direction[1];

        while (this.board[newX][newY] != player) {
            this.board[newX][newY] = player;
            newX += direction[0];
            newY += direction[1];
        }
    }

    /**
     * Updates the score based on the current board state.
     */
    updateScore() {
        const scores = this.board.flatMap(row => row).reduce((acc, val) => {
            acc[val]++;
            return acc;
        }, [0, 0, 0]);

        this.score.no = scores[0];
        this.score.black = scores[1];
        this.score.white = scores[2];
    }

    /**
     * Gets the current score.
     * @returns {Object} The score object with counts for white, black, and empty tiles.
     */
    getScore() {
        return this.score;
    }
}