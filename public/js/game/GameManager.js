import Board from "./Board.js";
import HumanPlayer from "./players/HumanPlayer.js";
import RandomPlayer from "./players/RandomPlayer.js";
import CostPlayer from "./players/CostPlayer.js";
import LeaderboardManager from "../api-managers/LeaderboardManager.js";

/**
 * Class representing a game manager for handling the Reversi game.
 */
export default class GameManager {
    /**
     * Creates an instance of GameManager.
     * @param {string} player1 - The type or name of player 1.
     * @param {string} player2 - The type or name of player 2.
     * @param {boolean} showHelp - Flag to indicate whether to show help during the game.
     */
    constructor(player1, player2, showHelp) {
        this.board = new Board(showHelp); 
        this.player1 = player1;
        this.player2 = player2;

        if (player1 === "random") {
            this.player1 = new RandomPlayer(this.board, 1);
        } else if (player1 === "cost") {
            this.player1 = new CostPlayer(this.board, 1);
        } else {
            this.player1 = new HumanPlayer(this.board, 1, player1);
        }

        if (player2 === "random") {
            this.player2 = new RandomPlayer(this.board, 2);
        } else if (player2 === "cost") {
            this.player2 = new CostPlayer(this.board, 2);
        } else {
            this.player2 = new HumanPlayer(this.board, 2, player2);
        }
        
        this.activePlayer = 1;
        this.animationFrameId = null; 
        this.isCanvasVisible = false; 

        this.gameTime = 0;
        this.timerInterval = null;

        this.handleUserClick = this.handleUserClick.bind(this);
        this.originalTitle = document.title;

        // Event listener for visibility change
        document.addEventListener('visibilitychange', this.handleVisibilityChange.bind(this));
        this.gameActive = false;
    }

    /**
     * Handles visibility change events to pause or resume the game timer.
     */
    handleVisibilityChange() {
        if (document.visibilityState === 'visible' && this.gameActive) {
            // Resume timer and update title
            this.resumeTimer();
            document.title = this.originalTitle;
        } else if (this.gameActive) {
            // Pause timer and update title
            this.pauseTimer();
            document.title = `Hra pozastavena (${this.secondsToTimeString(this.gameTime)})`;
        }
    }

    /**
     * Pauses the game timer.
     */
    pauseTimer() {
        clearInterval(this.timerInterval);
    }

    /**
     * Resumes the game timer.
     */
    resumeTimer() {
        this.timerInterval = setInterval(() => {
            this.gameTime++;
            document.getElementById('gameTime').textContent = `Čas hry: ${this.secondsToTimeString(this.gameTime)}`;
        }, 1000);
    }

    /**
     * Updates the game state and handles game logic.
     */
    update() {
        this.checkCanvasVisibility();
        if (this.board.getScore().no > 0 && this.isCanvasVisible && this.board.canAnyonePlay()) {
            if (this.activePlayer === 1 && this.player1.getType() !== "human") {
                this.player1.playMove();
                this.activePlayer = 2;
            } else if (this.activePlayer === 2 && this.player2.getType() !== "human") {
                this.player2.playMove();          
                this.activePlayer = 1;
            } else if (this.board.getValidMoves(this.activePlayer).length === 0) {
                this.activePlayer = (this.activePlayer === 1) ? 2 : 1;
            }  

            this.board.drawCompleteBoard(this.activePlayer);
            this.updateGameInfo();

            this.animationFrameId = requestAnimationFrame(this.update.bind(this));
        } else {
            if (this.animationFrameId) {
                this.gameActive = false;

                const canvas = document.getElementById('gameBoard');
                if (canvas) {
                    canvas.remove();
                }

                const gameInf = document.getElementById('game-info');
                if (gameInf) {
                    gameInf.remove();
                }
                
                cancelAnimationFrame(this.animationFrameId);
                this.animationFrameId = null;

                // Pause timer and save winner
                this.pauseTimer();
                const winner = this.saveWinner();

                this.showFinalPage(winner);

                // Reset timer and game time
                this.gameTime = 0;
                this.timerInterval = null;
            }
        }
    }

    /**
     * Displays the final page with the game result.
     * @param {number} winner - The winner of the game (0 for computer, 1 for player1, 2 for player2).
     */
    showFinalPage(winner) {
        const html = `<section id="end-screen">
                        <h2>Konec hry, </h2>
                        <p id="add-inf"></p>
                        <p>Děkujeme za hraní!</p>
                        <a href="${document.location.search}">Restartovat hru</a>

                        <audio id="winSound">
                            <source src="../sounds/win.wav" type="audio/wav">
                            Your browser does not support the audio element.
                        </audio>
                        <audio id="loseSound">
                            <source src="../sounds/lose.wav" type="audio/wav">
                            Your browser does not support the audio element.
                        </audio>
                    </section>`;
    
        const gamePanelElement = document.getElementById('game-panel');
        if (gamePanelElement) {
            gamePanelElement.innerHTML += html;
    
            const endscreen = document.getElementById("end-screen");
            if (endscreen) {
                const heading = endscreen.querySelector('h2');
                if (heading) {
                    if (winner === 0) {
                        heading.textContent += "vyhrál počítač.";
                    } else if (winner === 1) {
                        heading.textContent += `vyhrál ${this.player1.getNickname()}!`;
                    } else if (winner === 2) {
                        heading.textContent += `vyhrál ${this.player2.getNickname()}!`;
                    }
                }

                const score = this.board.getScore();
                const infoElement = document.getElementById('add-inf');
                if (infoElement) {
                    infoElement.textContent = `Skóre: ${score.black} vs. ${score.white}, čas: ${this.gameTime}`;
                }
        
                if (winner === 1 || winner === 2) {
                    const audio = document.getElementById('winSound');
                    audio.play(); 
                } else if (winner === 0) {
                    const audio = document.getElementById('loseSound');
                    audio.play();
                }
            }
        }
    }

    /**
     * Saves the winner to the leaderboard if they are a human player.
     * @returns {number} - The winner of the game (0 for computer, 1 for player1, 2 for player2).
     */
    saveWinner() {
        const leaderboardManager = new LeaderboardManager();
        const winner = this.board.getScore().black > this.board.getScore().white ? 1 : 2;

        if (winner === 1 && this.player1.getType() === "human") {
            const nickname = this.player1.getNickname();
            const score = this.board.getScore().black;
            const time = this.gameTime;
            leaderboardManager.addPlayerToLeaderboard(nickname, score, time);
            return winner;
        } else if (winner === 2 && this.player2.getType() === "human") {
            const nickname = this.player2.getNickname();
            const score = this.board.getScore().white;
            const time = this.gameTime;
            leaderboardManager.addPlayerToLeaderboard(nickname, score, time);
            return winner;
        }
        
        return 0;
    }

    /**
     * Starts the game by drawing the board and setting up the timer and event listeners.
     */
    start() {
        this.board.drawCompleteBoard(this.activePlayer);
        this.board.canvas.addEventListener('click', this.handleUserClick);
        this.gameActive = true;
        this.startTimer();
        this.update();
    }

    /**
     * Closes the game by removing event listeners and stopping the timer.
     */
    close() {
        this.board.canvas.removeEventListener('click', this.handleUserClick);
        clearInterval(this.timerInterval); 
        document.removeEventListener('visibilitychange', this.handleVisibilityChange.bind(this));
    }

    /**
     * Checks if the game canvas is visible on the page.
     */
    checkCanvasVisibility() {
        this.isCanvasVisible = !!document.querySelector('#gameBoard');
    }

    /**
     * Handles user click events for making moves.
     * @param {Event} event - The click event object.
     */
    handleUserClick(event) {
        if (this.activePlayer === 1 && this.player1.getType() === "human") {
            let played = this.player1.playMove(event);
            if (played === 1) {
                this.activePlayer = 2;
            }
        } else if (this.activePlayer === 2 && this.player2.getType() === "human") {
            let played = this.player2.playMove(event);          
            if (played === 1) {
                this.activePlayer = 1;
            }
        }
    }

    /**
     * Converts seconds to a formatted time string.
     * @param {number} seconds - The number of seconds to convert.
     * @returns {string} - The formatted time string.
     */
    secondsToTimeString(seconds) {
        const minutes = Math.floor(seconds / 60);
        const remainingSeconds = seconds % 60;

        let timeString = '';
        if (minutes > 0) {
            timeString += minutes + 'm ';
        }
        if (remainingSeconds > 0) {
            timeString += remainingSeconds + 's';
        }

        return timeString.trim();
    }

    /**
     * Starts the game timer and updates the timer display every second.
     */
    startTimer() {
        this.gameTime = 0;
        this.timerInterval = setInterval(() => {
            this.gameTime++;
            if (document.getElementById('gameTime')) {
                document.getElementById('gameTime').textContent = `Čas hry: ${this.secondsToTimeString(this.gameTime)}`;
            }
        }, 1000);
    }

    /**
     * Updates the game information display with the current player and score.
     */
    updateGameInfo() {
        let name1 = "Počítač";
        let name2 = "Počítač";
        
        if (this.player2.getType() === "human") {
            name2 = this.player2.getNickname();
        }
        if (this.player1.getType() === "human") {
            name1 = this.player1.getNickname();
        }

        let name = (this.activePlayer === 1) ? name1 : name2;
        const score = this.board.getScore();
        const currentPlayerElement = document.getElementById('currentPlayer');
        if (currentPlayerElement) {
            currentPlayerElement.textContent = `Hráč na řadě: ${name}`;
        }

        const gameScoreElement = document.getElementById('gameScore');
        if (gameScoreElement) {
            gameScoreElement.textContent = `Skóre - ${name1}: ${score.black} vs. ${name2}: ${score.white}`;
        }
    }
}
