import AbstractPlayer from "./AbstractPlayer.js";

/**
 * Represents a player in the game who makes moves based on a heuristic cost function.
 * @extends AbstractPlayer
 */
export default class CostPlayer extends AbstractPlayer {
    /**
     * Creates an instance of a cost-based player.
     * @param {Object} board - The game board.
     * @param {Object} player - The player information.
     */
    constructor(board, player) {
        super(board, player);
        this.validMoves = [];
    }

    /**
     * Plays a move for the player based on the heuristic values of the board positions.
     * The player chooses the move with the highest heuristic value.
     */
    playMove() {
        const boardValues = [
            [100, -20, 10, 5, 5, 10, -20, 100],
            [-20, -50, -2, -2, -2, -2, -50, -20],
            [10, -2, -1, -1, -1, -1, -2, 10],
            [5, -2, -1, -1, -1, -1, -2, 5],
            [5, -2, -1, -1, -1, -1, -2, 5],
            [10, -2, -1, -1, -1, -1, -2, 10],
            [-20, -50, -2, -2, -2, -2, -50, -20],
            [100, -20, 10, 5, 5, 10, -20, 100]
        ];

        this.validMoves = this.board.getValidMoves(this.player);
        let max = -Infinity;
        let bestMoves = [];

        for (let i = 0; i < this.validMoves.length; ++i) {
            let move = this.validMoves[i];
            if (max < boardValues[move[0]][move[1]]) {
                max = boardValues[move[0]][move[1]];
                bestMoves = [move];
            } else if (max == boardValues[move[0]][move[1]]) {
                bestMoves.push(move);
            }
        }

        if (bestMoves.length > 0) {
            let move = bestMoves[Math.floor(Math.random() * bestMoves.length)];
            this.board.playPlayersMove(move, this.player);
        }
    }
    
    /**
     * Gets the type of the player.
     * @returns {string} The type of the player, which is "cost".
     */
    getType() {
        return "cost";
    }
}
