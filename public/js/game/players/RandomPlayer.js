import AbstractPlayer from "./AbstractPlayer.js";

/**
 * Represents a player that makes random moves.
 * @extends AbstractPlayer
 */
export default class RandomPlayer extends AbstractPlayer {
    /**
     * Creates an instance of a random move player.
     * @param {Object} board - The game board.
     * @param {Object} player - The player information.
     */
    constructor(board, player) {
        super(board, player);
        this.validMoves = [];
    }

    /**
     * Plays a random move from the list of valid moves.
     */
    playMove() {
        this.validMoves = this.board.getValidMoves(this.player);

        if (this.validMoves.length > 0) {
            let move = this.validMoves[Math.floor(Math.random() * this.validMoves.length)];
            this.board.playPlayersMove(move, this.player);
        }
    }
    
    /**
     * Gets the type of the player.
     * @returns {string} The type of the player, which is "random".
     */
    getType() {
        return "random";
    }
}
