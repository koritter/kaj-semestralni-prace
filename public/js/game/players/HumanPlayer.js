import AbstractPlayer from "./AbstractPlayer.js";

/**
 * Represents a human player in the game.
 * @extends AbstractPlayer
 */
export default class HumanPlayer extends AbstractPlayer {
    /**
     * Creates an instance of a human player.
     * @param {Object} board - The game board.
     * @param {Object} player - The player information.
     * @param {string} nickname - The nickname of the player.
     */
    constructor(board, player, nickname) {
        super(board, player);
        this.validMoves = [];
        this.nickname = nickname;
    }

    /**
     * Plays a move for the player based on the mouse event.
     * @param {MouseEvent} event - The mouse event triggered by a player action.
     * @returns {number} Returns 1 if the move is valid and played, otherwise returns 0.
     */
    playMove(event) {
        const rect = this.board.canvas.getBoundingClientRect();
        const y = (event.clientX - rect.left) / 50 | 0;
        const x = (event.clientY - rect.top) / 50 | 0;

        this.validMoves = this.board.getValidMoves(this.player);

        if (this.validMoves.some((el) => el[0] == x && el[1] == y)) {
            this.board.playPlayersMove([x, y], this.player);
            return 1;
        }

        return 0;
    }
    
    /**
     * Gets the type of the player.
     * @returns {string} The type of the player, which is "human".
     */
    getType() {
        return "human";
    }

    /**
     * Gets the nickname of the player.
     * @returns {string} The nickname of the player.
     */
    getNickname() {
        return this.nickname;
    }
}
