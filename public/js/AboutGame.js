import AbstractView from "./AbstractView.js";

/**
 * Represents the view for the "About Game" section, providing details about the game Reversi.
 * @extends AbstractView
 */
export default class AboutGame extends AbstractView {
    /**
     * Initializes the view with the given settings.
     * @param {Object} settings - The settings for the view.
     */
    constructor(settings) {
        super(settings);
    }

    /**
     * Returns the HTML content for the "About Game" section.
     * @returns {string} The HTML content.
     */
    getHtml() {
        return `
        <section id="about-game">
            <article>
                <h1>Reversi: Hra strategie a taktiky</h1>
                <p>Reversi, známá také jako Othello, je strategická desková hra pro dva hráče, která se hraje na čtvercové hrací desce o velikosti 8x8. Tato hra kombinuje jednoduchá pravidla s hlubokou strategií a taktikou, což ji činí oblíbenou jak mezi rekreačními hráči, tak i mezi vášnivými hráči turnajové úrovně.</p>
                <p>Cílem hry je získat co nejvíce kamínků své barvy na hrací desce po skončení hry. Hráči se střídají v umisťování svých kamínků na volná pole hrací desky. Kámen může být umístěn pouze na pole, které sousedí s polem obsazeným soupeřovým kamenem a ve kterém je alespoň jeden soupeřův kámen na řadě mezi tímto nově umístěným kamenem a jiným vlastním kamenem. Po umístění kamene se všechny soupeřovy kameny, které jsou mezi nově umístěným kamenem a jiným vlastním kamenem ve stejném řádku, sloupci nebo diagonále, převrátí na hráčovu barvu.</p>
                <p>Strategie hry spočívá v tom, jak efektivně obsadit a udržet kontrolu nad klíčovými oblastmi hrací desky, zatímco se snažíte minimalizovat možnosti vašeho soupeře. Dobře umístěný kámen může mít vliv na celou budoucnost hry a rozhodnout o vítězství nebo porážce. Rychlé přemýšlení, schopnost předvídat tahy soupeře a schopnost adaptovat se na měnící se situaci jsou klíčové pro úspěch.</p>
                <p>Hra končí, když už nelze na hrací desce umístit žádné další kameny, nebo když jsou všechna pole obsazena. Poté se spočítají kamínky na hrací desce a vyhrává hráč, který má na konci hry více kamínků své barvy. Reversi je takovou hrou, která je snadno naučitelná, ale obtížně ovladatelná, což ji činí výzvou pro hráče všech úrovní dovedností.</p>

                <h2>Pravidla hry: podrobněji</h2>
                
                <h3>Počáteční rozložení</h3>
                <p>Hra Reversi začíná s prázdnou hrací deskou o rozměrech 8x8. Do středu této hrací desky se umístí čtyři kamínky, dva černé a dva bílé, tak aby byly v diagonálním uspořádání. Bílé kameny jdou na pole d4 a e5, černé na pole d5 a e4. Hru začíná hráč s černými kameny.</p>
                <figure>
                <img src="./svgs/initboard.svg" alt="Startovní deska">
                <figcaption>Počáteční rozložení kamenů na desce</figcaption>
                </figure>

                <h3>Průběh hry</h3>
                <p>Hráči se střídají v umisťování svých kamenů na hrací desku. Hráč může v každém tahu umístit jeden kámen a to tak, že ho položí na políčko vedle soupeřova kamene (i diagonálně). Položením kamene musí mezi svůj nový kámen a některý ze svých již položených uzavřít alespoň jeden soupeřův kámen. Po umístění kamene se všechny soupeřovy kameny, které jsou mezi nově umístěným kamenem a jiným vlastním kamenem ve stejném řádku, sloupci nebo diagonále, převrátí na barvu hráče, který zahrál tah.</p>
                <p>V níže přiložené ukázce je na řadě bílý hráč. Ten má možnost hrát na políčka e2, b3, c3, f3, b4, f4, b5, g5 a g6 (zeleně označená pole). Ze všech možností si zvolí g6 a otočí tak dva černé kameny umístěné na e4 a f5.</p>
                <figure>
                <img src="./svgs/showmove.svg" alt="Ukázka tahu">
                <figcaption>Ukázka hry</figcaption>
                </figure>

                <h3>Konec hry</h3>
                <p>Hra končí, když už nelze na hrací desce umístit žádné další kamínky, nebo když jsou všechna pole obsazena. Vyhrává hráč, který má na konci hry více kamínků své barvy na hrací desce.</p>

                <h2>Fungování této hry</h2>
                <p>V této verzi hry Reversi má hráč na výběr ze dvou možností - Singleplayer nebo Multiplayer.</p>
                <p>Verze Singleplayer je určena pro jednoho hráče. Hráč si zde na začátku volí svou přezdívku (ta smí obsahovat jen písmena bez háčků a čárek a čísla) a soupeře, proti kterému chce hrát. Zároveň je zde možnost nechat si při hře zapnutou nápovědu, která bude ukazovat, která políčka jsou pro daného hráče v současném tahu hratelná. Po vyplnění formuláře se otevře herní deska a hráč může začít hrát. Kameny umisťuje kliknutím myši na políčko.</p>
                <p>Verze Multiplayer je určena pro dva hráče, kteří se v tazích střídají. Na začátku je opět třeba vyplnit přezdívku obou dvou a zároveň si vybrat, zda chtějí hrát s nápovědou nebo bez.</p>
                <p>Nejlepší dosažená skóre se ukládají a hráči si je mohou zobrazit v nejlepších hráčích.</p>

            </article>
        </section>
        `;
    }
}
