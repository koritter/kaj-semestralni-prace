import AbstractView from "./AbstractView.js";
import LeaderboardManager from "./api-managers/LeaderboardManager.js";

/**
 * Represents the view for displaying the best players leaderboard.
 * @extends AbstractView
 */
export default class BestPlayers extends AbstractView {
    /**
     * Initializes the BestPlayers view.
     * @param {Object} parameters - The parameters for the view.
     */
    constructor(parameters) {
        super(parameters);
        this.setTitle("Reversi");
    }

    /**
     * Returns the HTML content for the leaderboard view.
     * @returns {string} The HTML content.
     */
    getHtml() {
        return `
            <section id="leaders-panel">
                <h1>TOP 10 nejlepších hráčů</h1>
                <svg id="leaderboardSvg" width="450" height="380"></svg>
            </section>
        `;
    }

    /**
     * Shows the page and displays the leaderboard.
     * @param {string} url - The URL of the page.
     */
    pageShow(url) {
        super.pageShow(url);
        this.displayLeaderboard();
    }

    /**
     * Creates an SVG header cell.
     * @param {string} displayText - The text to display in the cell.
     * @param {number} x - The x-coordinate of the cell.
     * @param {number} y - The y-coordinate of the cell.
     * @param {number} cellWidth - The width of the cell.
     * @param {number} rowHeight - The height of the cell.
     * @returns {SVGGElement} The created SVG group element.
     */
    createHeaderCell(displayText, x, y, cellWidth, rowHeight) {
        const cellGroup = document.createElementNS('http://www.w3.org/2000/svg', "g");
        cellGroup.setAttribute("class", "header-cell");

        const cell = document.createElementNS('http://www.w3.org/2000/svg', "rect");
        cell.setAttribute("class", "header-cell-bg");
        cell.setAttribute("x", x);
        cell.setAttribute("y", y);
        cell.setAttribute("width", cellWidth);
        cell.setAttribute("height", rowHeight);
        cell.setAttribute("fill", "rgba(65,161,137,0.7)");

        const text = document.createElementNS('http://www.w3.org/2000/svg', "text");
        text.setAttribute("x", x + cellWidth / 2);
        text.setAttribute("y", y + rowHeight / 2);
        text.setAttribute("dy", ".35em");
        text.setAttribute("text-anchor", "middle");
        text.textContent = displayText;

        cellGroup.append(cell);
        cellGroup.append(text);

        return cellGroup;
    }

    /**
     * Creates an SVG cell.
     * @param {string} displayText - The text to display in the cell.
     * @param {number} x - The x-coordinate of the cell.
     * @param {number} y - The y-coordinate of the cell.
     * @param {number} cellWidth - The width of the cell.
     * @param {number} rowHeight - The height of the cell.
     * @returns {SVGGElement} The created SVG group element.
     */
    createCell(displayText, x, y, cellWidth, rowHeight) {
        const cellGroup = document.createElementNS('http://www.w3.org/2000/svg', "g");
        cellGroup.setAttribute("class", "cell");

        const cell = document.createElementNS('http://www.w3.org/2000/svg', "rect");
        cell.setAttribute("class", "cell-bg");
        cell.setAttribute("x", x);
        cell.setAttribute("y", y);
        cell.setAttribute("width", cellWidth);
        cell.setAttribute("height", rowHeight);
        cell.setAttribute("fill", "rgba(105,238,204,0.7)");

        const text = document.createElementNS('http://www.w3.org/2000/svg', "text");
        text.setAttribute("x", x + cellWidth / 2);
        text.setAttribute("y", y + rowHeight / 2);
        text.setAttribute("dy", ".35em");
        text.setAttribute("text-anchor", "middle");
        text.textContent = displayText;

        cellGroup.append(cell);
        cellGroup.append(text);

        return cellGroup;
    }

    /**
     * Converts seconds to a time string in the format of "Xm Ys".
     * @param {string} secondsString - The number of seconds as a string.
     * @returns {string} The formatted time string.
     */
    secondsToTimeString(secondsString) {
        const seconds = Number(secondsString);
        const minutes = Math.floor(seconds / 60);
        const remainingSeconds = seconds % 60;

        let timeString = '';
        if (minutes > 0) {
            timeString += minutes + 'm ';
        }
        if (remainingSeconds > 0) {
            timeString += remainingSeconds + 's';
        }

        return timeString.trim();
    }

    /**
     * Creates an SVG row for a leaderboard entry.
     * @param {Object} entry - The leaderboard entry.
     * @param {number} index - The index of the entry.
     * @param {number} rowHeight - The height of the row.
     * @param {number} y - The y-coordinate of the row.
     * @returns {SVGGElement} The created SVG group element.
     */
    createRow(entry, index, rowHeight, y) {
        const row = document.createElementNS('http://www.w3.org/2000/svg', "g");
        row.setAttribute("class", "row");
        row.setAttribute("transform", `translate(0, ${y})`);

        const handleMouseOver = () => {
            row.style.filter = "drop-shadow(0px 0px 10px #44d9b49f)";
            row.querySelector(".cell-bg").setAttribute("fill", "rgba(105,238,204,0.9)");
        };

        const handleMouseOut = () => {
            row.style.filter = "none";
            row.querySelector(".cell-bg").setAttribute("fill", "rgba(105,238,204,0.7)");
        };

        row.addEventListener("mouseover", handleMouseOver);
        row.addEventListener("mouseout", handleMouseOut);

        const orderCell = this.createCell(`${index}`, 0, 0, 20, rowHeight);
        const nameCell = this.createCell(`${entry.name}`, 25, 0, 200, rowHeight);
        const scoreCell = this.createCell(`${entry.score}`, 230, 0, 100, rowHeight);
        const timeCell = this.createCell(`${this.secondsToTimeString(entry.time)}`, 335, 0, 115, rowHeight);

        row.append(orderCell);
        row.append(nameCell);
        row.append(scoreCell);
        row.append(timeCell);

        return row;
    }

    /**
     * Creates an SVG header row.
     * @param {number} rowHeight - The height of the row.
     * @param {number} y - The y-coordinate of the row.
     * @returns {SVGGElement} The created SVG group element.
     */
    createHeaderRow(rowHeight, y) {
        const row = document.createElementNS('http://www.w3.org/2000/svg', "g");
        row.setAttribute("class", "header-row");
        row.setAttribute("transform", `translate(0, ${y})`);

        const orderCell = this.createHeaderCell(``, 0, 0, 20, rowHeight);
        const nameCell = this.createHeaderCell(`Přezdívka`, 25, 0, 200, rowHeight);
        const scoreCell = this.createHeaderCell(`Skóre`, 230, 0, 100, rowHeight);
        const timeCell = this.createHeaderCell(`Čas:`, 335, 0, 115, rowHeight);

        row.append(orderCell);
        row.append(nameCell);
        row.append(scoreCell);
        row.append(timeCell);

        return row;
    }

    /**
     * Displays the leaderboard by creating and appending SVG rows.
     */
    displayLeaderboard() {
        const leaderboardManager = new LeaderboardManager();
        const leaderboard = leaderboardManager.getLeaderboard();

        const svg = document.querySelector('#leaderboardSvg');
        svg.innerHTML = '';

        const rowHeight = 30;
        const padding = 5;

        const tableHeader = this.createHeaderRow(rowHeight, 0);
        svg.append(tableHeader);

        leaderboard.forEach((entry, index) => {
            const y = (index + 1) * (rowHeight + padding);
            const row = this.createRow(entry, (index + 1), rowHeight, y);
            svg.append(row);
        });
    }
}
