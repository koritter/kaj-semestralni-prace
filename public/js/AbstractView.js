import SideNav from "./SideNav.js";
import FullscreenManager from "./api-managers/FullscreenManager.js";

/**
 * Abstract class representing a view in the application.
 */
export default class AbstractView {
    /**
     * Creates an instance of the AbstractView class.
     * @param {Object} options - Options for creating the view.
     * @param {string} options.key - The key of the view.
     * @param {string} options.title - The title of the view.
     * @param {boolean} [options.shownav=true] - Indicates whether the navigation bar should be shown.
     */
    constructor({key, title, shownav = true}) {
        this.pageElement = document.querySelector("#app")
        this.title = title
        this.key = key
        this.nav = new SideNav();
        this.shownav = shownav;
    }

    /**
     * Sets the title of the view.
     */
    setTitle() {
        document.title = this.title;
    }

    /**
     * Generates the HTML content for the view.
     * @returns {string} The HTML content.
     */
    getHtml() {
        return ``;
    }

    /**
     * Displays the view.
     * @param {URL} url - The URL of the page.
     */
    pageShow(url) {
        this.setTitle()
        if (this.shownav) {
            this.pageElement.innerHTML = this.nav.getHtml() + this.getHtml();

            this.setActiveNav(url);

            new FullscreenManager();
        } else {
            this.pageElement.innerHTML = this.getHtml();
        }
    }

    /**
     * Hides the view.
     */
    pageHide() {
        this.unsetActiveNav();
        this.pageElement.innerHTML = '';
    }

    /**
     * Sets the active navigation link based on the current URL.
     * @param {URL} url - The URL of the page.
     */
    setActiveNav(url) {
        let queryString = url.search;
        let splitedQS = queryString.split(/=|&/);
        
        let pageParameterValue = splitedQS[splitedQS.indexOf("?page") + 1];

        const links = document.querySelectorAll('aside a');

        links.forEach(link => {
            if (link.id === pageParameterValue) {
                link.classList.add('active'); 
            }
        });
    }

    /**
     * Unsets the active navigation link.
     */
    unsetActiveNav() {
        let url = new URL(window.location.href);
        let page = url.searchParams.get('page');
       
        let links = document.querySelectorAll('aside a');

        links.forEach(link => {
            if (link.id === page) {
                link.classList.remove('active'); 
            }
        });
    }
}
