import AbstractView from "./AbstractView.js";
import GameManager from "./game/GameManager.js";

/**
 * View class representing the multiplayer game setup form.
 * Extends AbstractView.
 */
export default class MultiPlayer extends AbstractView {
    /**
     * Creates an instance of the MultiPlayer class.
     * @param {Object} parameters - Parameters for the view.
     */
    constructor(parameters) {
        super(parameters);
        this.setTitle("Reversi");
    }

    /**
     * Generates HTML content for the multiplayer game setup form.
     * @returns {string} The HTML content.
     */
    getHtml() {
        return `
        <section id="pregame-formular">
        <h1>Formulář s nastavením hry</h1>
            <form id="game-form">
                <label for="nickname1">Hráč 1:<span class="mandatory"> *</span>:</label>
                <input type="text" id="nickname1" name="nickname1" placeholder="Zadejte vaši přezdívku" required autofocus tabindex="1" accesskey="o" pattern="[a-zA-Z0-9_]+">
                
                <label for="nickname2">Hráč 2:<span class="mandatory"> *</span>:</label>
                <input type="text" id="nickname2" name="nickname2" placeholder="Zadejte vaši přezdívku" required tabindex="2" accesskey="t" pattern="[a-zA-Z0-9_]+">
                                               
                <label for="show_help">Zobrazit při hře nápovědu?</label>
                <input type="checkbox" id="show_help" name="show_help" accesskey="h">
                <label for="show_help" id="show-help-label"></label>
                
                <input type="submit" value="Odeslat" tabindex="3" accesskey="s">
            </form>
        </section>
        `;
    }

    /**
     * Event handler for when the page is shown.
     * @param {string} url - The URL of the page.
     */
    pageShow(url) {
        super.pageShow(url);

        let gameSettings = {
            "firstplayer": "",
            "secondplayer": "",
            "showHelp": false
        };

        document.getElementById("game-form").addEventListener("submit",(e) => {this.processInitForm(e, gameSettings)});

        const autoFocusInput = document.querySelector("input[autofocus]");
        autoFocusInput.focus();
    }

    /**
     * Processes the form submitted for initializing the multiplayer game.
     * @param {Event} e - The submit event.
     * @param {Object} gameSettings - The game settings object to be updated.
     */
    processInitForm(e, gameSettings) {
        e.preventDefault();
        
        let player1 = document.getElementById("nickname1").value;
        gameSettings.firstPlayer = player1.trim();

        let player2 = document.getElementById("nickname2").value;
        gameSettings.secondPlayer = player2.trim();

        let showHelp = document.getElementById("show_help").checked;
        gameSettings.showHelp = showHelp;

        this.createGameManager(gameSettings);
    }

    /**
     * Creates a new game manager based on the provided game settings and starts the game.
     * @param {Object} gameSettings - The game settings.
     */
    createGameManager(gameSettings) {
        document.getElementById("pregame-formular").remove();
        let canvasHtml = `
            <section id="game-panel">
                <h1>Reversi - multiplayer</h1>
                <canvas id="gameBoard" width="400" height="400"></canvas>
                <section id="game-info">
                    <h3>Herní status:</h3>
                    <p id="gameTime">Čas hry: 0s</p>
                    <p id="currentPlayer">Hráč na řadě:</p>
                    <p id="gameScore">Skóre - Black: 2 vs White: 2</p>
                </section>
            </section>
            `;
        let mainElement = document.querySelector("#app");
        mainElement.insertAdjacentHTML('beforeend', canvasHtml);

        let gameManager = new GameManager(gameSettings.firstPlayer, gameSettings.secondPlayer, gameSettings.showHelp); 
        gameManager.start();
    }
}
