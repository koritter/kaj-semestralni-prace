import AbstractView from "./AbstractView.js";
import GameManager from "./game/GameManager.js";

/**
 * View class representing the pregame form for setting up game parameters.
 * Extends AbstractView.
 */
export default class SinglePlayer extends AbstractView {
    /**
     * Creates an instance of the PregameFormView class.
     * @param {Object} parameters - Parameters for the view.
     */
    constructor(parameters) {
        super(parameters);
    }

    /**
     * Generates HTML content for the pregame form.
     * @returns {string} The HTML content.
     */
    getHtml() {
        return `
        
        <section id="pregame-formular">
            <h1>Formulář s nastavením hry</h1>
            <form id="game-form">
                <label for="nickname">Přezdívka<span class="mandatory"> *</span>:</label>
                <input type="text" id="nickname" name="nickname" placeholder="Zadejte vaši přezdívku" required autofocus tabindex="1" accesskey="p" pattern="[a-zA-Z0-9_]+">
                
                <label for="difficulty">Obtížnost soupeře:</label>
                <select id="difficulty" name="difficulty" tabindex="2" accesskey="e">
                    <option value="easy">Lehký</option>
                    <option value="hard">Obtížný</option>
                    <option value="rand-enemy">Zvolit náhodně</option>
                </select>

                <label>Kdo začne hru:</label>
                <section id="game-init-first-player">
                    <input type="radio" id="start_me" name="start" value="me" checked accesskey="m"> 
                    <label for="start_me">Já</label>
                    <input type="radio" id="start_comp" name="start" value="comp" accesskey="c">
                    <label for="start_comp">Počítač</label>
                    <input type="radio" id="start_rand" name="start" value="rand" accesskey="r">
                    <label for="start_rand">Náhodně</label>
                </section>
                                
                       
                <label for="show_help">Zobrazit při hře nápovědu?</label>
                <input type="checkbox" id="show_help" name="show_help" accesskey="h">
                <label for="show_help" id="show-help-label"></label>
                
                <input type="submit" value="Odeslat" tabindex="3" accesskey="s">
            </form>
        </section>
        `;
    }

    /**
     * Event handler for when the page is shown.
     * @param {string} url - The URL of the page.
     */
    pageShow(url) {
        super.pageShow(url);

        let gameSettings = {
            "nickaname": "",
            "enemy": "",
            "firstPlayer": 0,
            "showHelp": false
        };

        document.getElementById("game-form").addEventListener("submit",(e) => {this.processInitForm(e, gameSettings)});
        
        const autoFocusInput = document.querySelector("input[autofocus]");
        autoFocusInput.focus();
    }

    /**
     * Processes the form submitted for initializing the game.
     * @param {Event} e - The submit event.
     * @param {Object} gameSettings - The game settings object to be updated.
     */
    processInitForm(e, gameSettings) {
        e.preventDefault();
        
        let nickname = document.getElementById("nickname").value;
        gameSettings.nickaname = nickname.trim();

        let difficulty = document.getElementById("difficulty").value;
        if (difficulty == "easy") {
            gameSettings.enemy = "random";
        } else if (difficulty == "hard") {
            gameSettings.enemy = "cost";
        } else if(difficulty == "rand-enemy") {
            gameSettings.enemy = ["random", "cost"][Math.floor(Math.random() * 2)];
        } else {
            gameSettings.enemy = ["random", "cost"][Math.floor(Math.random() * 2)];
        }

        let start = document.querySelector('input[name="start"]:checked').value;
        if (start == "me") {
            gameSettings.firstPlayer = 1;
        } else if (start == "comp") {
            gameSettings.firstPlayer = 2;
        } else if(start == "start_rand") {
            gameSettings.firstPlayer = Math.floor(Math.random() * 2) + 1;
        } else {
            gameSettings.firstPlayer = Math.floor(Math.random() * 2) + 1;
        }

        let showHelp = document.getElementById("show_help").checked;
        gameSettings.showHelp = showHelp;

        this.createGameManager(gameSettings);
    }

    /**
     * Creates a new game manager based on the provided game settings and starts the game.
     * @param {Object} gameSettings - The game settings.
     */
    createGameManager(gameSettings) {
        document.getElementById("pregame-formular").remove();
        let canvasHtml = `
            <section id="game-panel">
                <h1>Reversi - singleplayer</h1>
                <canvas id="gameBoard" width="400" height="400"></canvas> 
                <section id="game-info">
                    <h3>Herní status:</h3>
                    <p id="gameTime">Čas hry: 0s</p>
                    <p id="currentPlayer">Hráč na řadě:</p>
                    <p id="gameScore">Skóre - Black: 2 vs White: 2</p>
                </section>
            </section>
            `;
        let mainElement = document.querySelector("#app");
        mainElement.insertAdjacentHTML('beforeend', canvasHtml);

        let player1 = "";
        let player2 = "";

        if (gameSettings.firstPlayer == 1) {
            player1 = gameSettings.nickaname;
            player2 = gameSettings.enemy;
        } else if (gameSettings.firstPlayer == 2) {
            player2 = gameSettings.nickaname;
            player1 = gameSettings.enemy;
        }

        let gameManager = new GameManager(player1, player2, gameSettings.showHelp); 
        gameManager.start();
    }
}
