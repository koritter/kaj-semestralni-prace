/**
 * Class representing a navigation menu.
 */
export default class {
    /**
     * Creates an instance of the navigation menu.
     */
    constructor() {
    }

    /**
     * Generates the HTML for the navigation menu.
     * @returns {string} The HTML string for the navigation menu.
     */
    getHtml() {
        return `
        <aside>
        <input type="checkbox" id="menu-toggle">
        <label for="menu-toggle" id="menu-label"></label>
            <nav>
                <ul>
                    <li><a href="?page=Home">Úvod</a></li>
                    <li><a href="?page=Singleplayer" id="Singleplayer">Singleplayer</a></li>
                    <li><a href="?page=Multiplayer" id="Multiplayer">Multiplayer</a></li>
                    <li><a href="?page=AboutGame" id="AboutGame">O hře</a></li>
                    <li><a href="?page=BestPlayers" id="BestPlayers">Nejlepší hráči</a></li>
                </ul>
            </nav>
            <button id="toggleFullscreenButton" >Zvětšit </button>
        </aside>
        `;
    }
}
