import HomePage from "./HomePage.js";
import SinglePlayer from "./SinglePlayer.js";
import MultiPlayer from "./Multiplayer.js";
import BestPlayers from "./BestPlayers.js";
import AboutGame from "./AboutGame.js";
import PageNotFound from "./PageNotFound.js";
import OfflineManager from "./api-managers/OfflineManager.js";

/**
 * Class representing a Router for handling navigation between different pages.
 */
class Router {
    /**
     * Creates an instance of the Router class.
     * @param {Object} config - Configuration for the router.
     * @param {Array} config.pages - Array of page instances to manage.
     * @param {string} config.defaultPage - The default page key to load if no specific page is found.
     */
    constructor({pages, defaultPage}) {
        this.pages = pages;
        this.defaultPage = defaultPage;
        this.currentPage = null;

        this.route(window.location.href);

        window.addEventListener('popstate', e => {
            this.route(window.location.href);
        });

        document.addEventListener("DOMContentLoaded", () => {
            window.addEventListener('click', e => {
                const element = e.target;
                if (element.nodeName === 'A') {
                    e.preventDefault();
                    this.route(element.href);
                    window.history.pushState(null, null, element.href);
                }
            });

            new OfflineManager();
        });
    }

    /**
     * Routes to the specified URL.
     * @param {string} urlString - The URL to route to.
     */
    route(urlString) {
        const url = new URL(urlString);
        const page = url.searchParams.get('page');

        if (this.currentPage) {
            this.currentPage.pageHide();
        }

        const page404 = this.pages.find(p => p.key === '404');
        const pageInstanceMatched = this.pages.find(p => p.key === (page || this.defaultPage));
        const currentPage = pageInstanceMatched ?? page404;

        this.currentPage = currentPage;
        this.currentPage.pageShow(url);
    }
}

new Router({
    pages: [
        new HomePage({key: 'Home', title: 'Reversi', shownav: false}),
        new AboutGame({key: 'AboutGame', title: 'Informace o hře'}),
        new SinglePlayer({key: 'Singleplayer', title: 'Singleplayer Reversi'}),
        new MultiPlayer({key: 'Multiplayer', title: 'Multiplayer Reversi'}),
        new BestPlayers({key: 'BestPlayers', title: 'Nejlepší hráči'}),
        new PageNotFound({key: '404', title: 'Page not found'})
    ],
    defaultPage: 'Home'
});
