import AbstractView from "./AbstractView.js";

/**
 * Class representing a "Page Not Found" view.
 * @extends AbstractView
 */
export default class PageNotFound extends AbstractView {
    
    /**
     * Creates an instance of the PageNotFound class.
     * @param {Object} settings - Settings for the view.
     */
    constructor(settings) {
        super(settings);
    }

    /**
     * Generates the HTML content for the "Page Not Found" view.
     * @returns {string} The HTML content.
     */
    getHtml() { 
        return `
                <h2>Not found</h2>
                <p>Neznámá strana. Vraťte se zpět.</p>
            `;
    }
}
