/**
 * Manages the application's behavior when it is offline.
 */
export default class OfflineManager {
    /**
     * Initializes the OfflineManager and sets up the necessary elements and event listeners.
     */
    constructor() {
        this.appElement = document.getElementById('app');
        this.alertBox = document.createElement('p');
        this.alertBox.id = 'offline-alert';
        this.alertBox.innerHTML = 'Není připojení k internetu.<br>Aplikace je pozastavena.';
        this.appElement.append(this.alertBox);

        this.links = document.querySelectorAll('a');
        this.forms = document.querySelectorAll('input[type="submit"]');
        
        this.registerNetworkStatusListeners();
    }

    /**
     * Displays the offline alert message.
     */
    showOfflineAlert() {
        this.alertBox.style.display = 'block';
    }

    /**
     * Hides the offline alert message.
     */
    hideOfflineAlert() {
        this.alertBox.style.display = 'none';
    }

    /**
     * Enables or disables the interaction with links and forms based on the provided parameter.
     * @param {boolean} enable - A flag indicating whether to enable or disable the links and forms.
     */
    enableLinksAndForms(enable) {
        this.links.forEach(link => {
            link.style.pointerEvents = enable ? 'auto' : 'none';
        });
        this.forms.forEach(button => {
            button.disabled = !enable;
        });
    }

    /**
     * Updates the application's online status. Shows or hides the offline alert and enables or disables links and forms.
     */
    updateOnlineStatus() {
        if (navigator.onLine) {
            this.hideOfflineAlert();
            this.enableLinksAndForms(true);
        } else {
            this.showOfflineAlert();
            this.enableLinksAndForms(false);
        }
    }

    /**
     * Registers event listeners to monitor the online and offline status of the application.
     */
    registerNetworkStatusListeners() {
        window.addEventListener('online', () => this.updateOnlineStatus());
        window.addEventListener('offline', () => this.updateOnlineStatus());
        this.updateOnlineStatus(); // Check initial status
    }
}
