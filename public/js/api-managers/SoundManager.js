/**
 * Manages sound playback using the Web Audio API.
 */
export default class SoundManager {
    /**
     * Creates an instance of the SoundManager class.
     */
    constructor() {
        /**
         * The audio context used for sound generation and processing.
         * @type {AudioContext}
         */
        this.audioContext = new (window.AudioContext || window.webkitAudioContext)();
        /**
         * The oscillator node used for generating sound.
         * @type {OscillatorNode|null}
         */
        this.oscillator = null;
        /**
         * A mapping of note names to frequencies.
         * @type {Object.<string, number>}
         */
        this.noteMap = {
            'C4': 261.63,
            'D4': 293.67,
            'E4': 329.63,
            'F4': 349.23,
            'G4': 392.00,
            'A4': 440.00,
            'B4': 493.88
        };
    }

    /**
     * Plays a note with the specified parameters.
     * @param {string} note - The note to play (e.g., 'C4', 'D4', etc.).
     * @param {number} noteModifier - The modifier for the note frequency (default is 1).
     * @param {OscillatorType} type - The type of oscillator waveform to use (e.g., 'sine', 'square', etc.).
     */
    playNote(note, noteModifier = 1, type = 'sine') {
        const frequency = this.noteToFrequency(note, noteModifier);

        if (this.oscillator) {
            this.oscillator.stop();
            this.oscillator.disconnect();
        }

        if (this.gainNode) {
            this.gainNode.gain.cancelScheduledValues(this.audioContext.currentTime);
            this.gainNode.gain.setValueAtTime(1, this.audioContext.currentTime);
            this.gainNode.gain.linearRampToValueAtTime(0.35, this.audioContext.currentTime + 0.35); 
        }

        this.oscillator = this.audioContext.createOscillator();
        this.oscillator.type = type; 
        this.oscillator.frequency.setValueAtTime(frequency, this.audioContext.currentTime);

        this.gainNode = this.audioContext.createGain();
        this.oscillator.connect(this.gainNode);
        this.gainNode.connect(this.audioContext.destination);

        this.oscillator.start();

        setTimeout(() => {
            this.oscillator.stop();
            this.oscillator.disconnect();
        }, 350); 
    } 

    /**
     * Converts a note name to its corresponding frequency.
     * @param {string} note - The note name (e.g., 'C4', 'D4', etc.).
     * @param {number} noteModifier - The modifier for the note frequency (default is 1).
     * @returns {number} The frequency of the note.
     */
    noteToFrequency(note, noteModifier) {
        let noteToReturn = this.noteMap[note];
        noteToReturn *= noteModifier;
        return noteToReturn;
    }
}
