/**
 * Manages the leaderboard for the game, handling loading, saving, and updating player scores.
 */
export default class LeaderboardManager {
    /**
     * Creates an instance of the LeaderboardManager class.
     */
    constructor() {
        /**
         * @property {string} leaderboardKey - The key used to store the leaderboard in localStorage.
         */
        this.leaderboardKey = 'gameLeaderboard';
        /**
         * @property {Array<Object>} leaderboard - The array representing the leaderboard.
         */
        this.leaderboard = this.loadLeaderboard();
    }

    /**
     * Loads the leaderboard from localStorage.
     * @returns {Array<Object>} The leaderboard array.
     */
    loadLeaderboard() {
        const leaderboardData = localStorage.getItem(this.leaderboardKey);
        if (leaderboardData) {
            return JSON.parse(leaderboardData);
        } else {
            return [];
        }
    }

    /**
     * Saves the leaderboard to localStorage.
     */
    saveLeaderboard() {
        localStorage.setItem(this.leaderboardKey, JSON.stringify(this.leaderboard));
    }

    /**
     * Adds a player to the leaderboard, sorts the leaderboard, and saves it.
     * @param {string} playerName - The name of the player.
     * @param {number} score - The score of the player.
     * @param {number} time - The time taken by the player.
     */
    addPlayerToLeaderboard(playerName, score, time) {
        this.leaderboard.push({ name: playerName, score: score, time: time });
        this.leaderboard.sort((a, b) => {
            if (a.score === b.score) {
                return a.time - b.time;
            }
            return b.score - a.score;
        });
        if (this.leaderboard.length > 10) {
            this.leaderboard.pop();
        }
        this.saveLeaderboard();
    }

    /**
     * Retrieves the leaderboard.
     * @returns {Array<Object>} The leaderboard array.
     */
    getLeaderboard() {
        return this.leaderboard;
    }
}
