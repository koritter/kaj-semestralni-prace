/**
 * Manages the fullscreen mode for the application.
 */
export default class FullscreenManager {
    /**
     * Creates an instance of FullscreenManager and initializes event listeners.
     */
    constructor() {
        this.element = document.querySelector('body');
        this.initEventListeners();
    }

    /**
     * Enters fullscreen mode for the element.
     */
    enterFullscreen() {
        if (this.element.requestFullscreen) {
            this.element.requestFullscreen();
        } else if (this.element.mozRequestFullScreen) { // Firefox
            this.element.mozRequestFullScreen();
        } else if (this.element.webkitRequestFullscreen) { // Chrome, Safari, Opera
            this.element.webkitRequestFullscreen();
        } else if (this.element.msRequestFullscreen) { // IE/Edge
            this.element.msRequestFullscreen();
        }
    }

    /**
     * Exits fullscreen mode.
     */
    exitFullscreen() {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        } else if (document.mozCancelFullScreen) { // Firefox
            document.mozCancelFullScreen();
        } else if (document.webkitExitFullscreen) { // Chrome, Safari, Opera
            document.webkitExitFullscreen();
        } else if (document.msExitFullscreen) { // IE/Edge
            document.msExitFullscreen();
        }
    }

    /**
     * Checks if the element is currently in fullscreen mode.
     * @returns {boolean} True if the element is in fullscreen mode, false otherwise.
     */
    isFullscreen() {
        return document.fullscreenElement === this.element ||
               document.mozFullScreenElement === this.element ||
               document.webkitFullscreenElement === this.element ||
               document.msFullscreenElement === this.element;
    }

    /**
     * Changes the text of the fullscreen toggle button.
     * @param {string} text - The text to set for the button.
     */
    changeText(text) {
        document.getElementById('toggleFullscreenButton').textContent = text;
    }

    /**
     * Initializes event listeners for the fullscreen toggle button.
     */
    initEventListeners() {
        document.getElementById('toggleFullscreenButton').addEventListener('click', () => {
            if (this.isFullscreen()) {
                this.changeText("Zvětšit");
                this.exitFullscreen();
            } else {
                this.changeText("Zmenšit");
                this.enterFullscreen();
            }
        });
    }
}
