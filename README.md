# KAJ - semestrální práce - Reversi

[Aplikace](https://kaj-semestralni-prace-koritter-e427c364784be9db9032904dcbe8ff95.pages.fel.cvut.cz)

## Cíl projektu
Cílem projektu bylo navrhnout a implementovat SPA aplikaci se hrou známou jako Reversi (Othello). Aplikace by měla umožňovat dva módy hry, multiplayer a singleplayer proti počítači, stejně jako volbu obtížnosti a zobrazení nápovědy. Dále by měla ukládat listinu deseti nejlepších hráčů a pracovat s multimediálním obsahem (zvuky).


## Postup
Aplikace byla vyvíjena následující posloupností činností:
1. Základní rozvržení aplikace (vytvoření sekcí, vytvoření routeru)
2. Implementace herní logiky
3. Přidání sekce o hře
4. Měření času, ukládání výsledků, sekce nejlepších hráčů
5. Přidání dalších API (fullscreen, offline, media, page visibility)
6. Finální úpravy vzhledu


## Popis funkčnosti
Aplikace, dostupná na odkazu v této dokumentaci, případně spustitelná po stažení repozitáře s rozšířením Live Server pro IDE Visual Studio Code, zobrazí po spuštění úvodní stránku.

Tato stránka obsahuje čtyři sekce. Sekce o hře obsahuje vysvětlená pravidla hry a krátký uživatelský návod, jak ovládat hru v této aplikaci. Sekce nejlepších hráčů obsahuje seznam deseti nejlepších hráčů (dle skóre a následně času) uloženou v LocalStorage a zobrazenou jako `<svg>` element.

Sekce singleplayer a multiplayer obsahují samotnou hru. Obě jsou uvozeny krátkým formulářem s herními možnostmi. V případě singleplayeru si uživatel volí svou přezdívku, obtížnost soupeře, kdo bude začínat a jestli zobrazit nápovědu. V případě multiplayeru oba hráči vyplní své přezdívky a volitelně si zapnou zobrazení herní nápovědy.

Po odeslání formuláře se na stránku přidá samotná hra. Herní plocha je tvořena značkou `<canvas>`, který je překreslován pomocí AnimationFrames. Při volbě zobrazení nápovědy se vykreslují navíc zelená kolečka ukazující, kde je možné hrát. Při nadjetí myší vydají zvuk, generovaný skrze Web Audio API. 

Pokud uživatel překlikne záložku s hrou, zastaví se čas hry pomocí Page Visibility API. Hra se obnoví, když se uživatel vrátí. Aplikaci lze rozšířit do celého okna (Fullscreen API) kliknutím na tlačítko zvětšit. V případě výpadku internetu se aplikace zablokuje a bude ji možné používat až bude připojení obnoveno (Offline API).

Po dohrání hry se obejví ukončující obrazovka a přehraje zvuk na základě toho, kdo vyhrál - jestli lidský uživatel či počítač (Media API). 

Aplikace má funkční přepínání historie mezi jednotlivými sekcemi a jejich přepání je prováděno třídou Router.

[Tabulka implementovaných funkčností](https://docs.google.com/spreadsheets/d/1Ku7hwSPqugFFPkvKWcrBbABMLQjyfOTfV6qoswRx6H4/edit?usp=sharing)
